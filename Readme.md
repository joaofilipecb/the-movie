﻿﻿# Cubos - The Movies

Desenvolvimento de aplicação (front-end) para a CUBOS.io. A aplicação consiste em: ***listar***, ***detalhar*** e ***pesquisar*** os principais filmes através da API: [https://developers.themoviedb.org](https://developers.themoviedb.org/)

### Bibliotecas utilizadas
 - node-sass 
 - react-dom 
 - react-redux 
 - react-router-dom 
 - redux
 - redux-thunk
 - axios
### Instalação e iniciação
Após ter dado um clone, a partir do repositório: https://bitbucket.org/joaofilipecb/the-movie/, entre no diretório da aplicação e execute os seguintes comandos:

 - `npm install`

 - `npm start`