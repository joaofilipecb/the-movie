import React, { useEffect, useState } from 'react';
import './Detail.scss';
import { useDispatch, useSelector } from 'react-redux';
import { listDetail, listSearchData } from '../actions/index';

function Detail() {
  const dispatch = useDispatch();
  const [currentRoute, setCurrentRoute] = useState(window.location.pathname);
  const detail = useSelector(state => state.listDetailData.data);

  useEffect(() => {
    currentRoute && convertRoute()
  }, [currentRoute]);

  const convertRoute = () => {
    let replaceRoute = currentRoute.replace("/detail/", "");
    dispatch(listDetail(replaceRoute))
  }

  return (
    <>
      <aside className="detail">
        <h1>{detail.title}</h1>
        <div className="left-detail">
          <div className="inside-detail">
            <div className="border">
              <h2>Sinopse</h2>
            </div>
            <p>{detail.overview}</p>
          </div>
          <div className="inside-detail">
            <div className="border">
              <h2>Informações</h2>
            </div>
            <div className="info">
              <div>
                <h2>Situação</h2>
                <p>{detail.status}</p>
              </div>
              <div>
                <h2>Idioma</h2>
                {}
              </div>
              <div>
                <h2>Duração</h2>
                <p>{detail.runtime} M</p>
              </div>
              <div>
                <h2>Orçamento</h2>
                <p>$ {detail.budget}</p>
              </div>
              <div>
                <h2>Receita</h2>
                <p>$ {detail.revenue}</p>
              </div>
          
            </div>
          </div>
        </div>
        <div className="right-detail">
          <figure><img src={`https://image.tmdb.org/t/p/w200/${detail.poster_path}`} /></figure>
        </div>
      </aside>
    </>
  )

}

export default Detail