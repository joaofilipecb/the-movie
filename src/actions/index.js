import axios from 'axios';



export const listMovies = (page) => {
    return dispatch => {
        axios.get(`https://api.themoviedb.org/3/discover/movie?api_key=cf63c596c9b5918baa8a6af7d2b7b249&sort_by=popularity.desc&page=${!page ? 1 : page}&language=pt-br`)
            .then(function (response) {
                dispatch({ type: 'LIST_MOVIES', data: response.data });
            })
            .catch(function (error) {

            })
            .then(function (data) {

            });
    }
}


export const listSearchMovies = (search) => {
    return dispatch => {
        axios.get(`https://api.themoviedb.org/3/search/movie?api_key=cf63c596c9b5918baa8a6af7d2b7b249&query=${search}`)
            .then(function (response) {
                dispatch({ type: 'LIST_SEARCH', data: response.data });
            })
            .catch(function (error) {

            })
            .then(function (data) {

            });
    }
}

export const listGenres = () => {
    return dispatch => {
        axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=cf63c596c9b5918baa8a6af7d2b7b249`)
            .then(function (response) {
                dispatch({ type: 'LIST_GENRES', data: response.data });
            })
            .catch(function (error) {

            })
            .then(function (data) {

            });
    }
}

export const listDetail = (detail) => {
    return dispatch => {
        axios.get(`https://api.themoviedb.org/3/movie/${detail}?api_key=cf63c596c9b5918baa8a6af7d2b7b249&language=pt-br`)
            .then(function (response) {
                dispatch({ type: 'LIST_DETAIL', data: response.data });
            })
            .catch(function (error) {

            })
            .then(function (data) {

            });
    }
}

export const setCurrentPage = (page) => {
    return {
        type: 'CURRENT_PAGE',
        data: page
    }
}

export const cleanSearch = {
    type: 'CLEAN_SEARCH',
    data: 'clean'
}

