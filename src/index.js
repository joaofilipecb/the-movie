import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers/';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Header from './Header/Header';
import Detail from './Detail/Detail';
import './GlobalAssets/Global.scss';
const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
);

ReactDOM.render(
<Provider store={store}>
    <BrowserRouter>
        <Header></Header>
        <Switch>
            <Route path="/" exact={true} component={App} />
            <Route path="/detail" component={Detail} />
        </Switch>
    </BrowserRouter>
</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
