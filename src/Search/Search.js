import React from 'react';
import './Search.scss';
import { listSearchMovies, cleanSearch} from '../actions/index';
import { useDispatch } from 'react-redux';
function Search() {
  const dispatch = useDispatch();

  const requestChange = (e) => {
    let timeOut;
    let getValue = e.currentTarget.value;
  
    e.currentTarget.onkeyup = (e) => {

      clearTimeout(timeOut);
      timeOut = setTimeout(() => dispatch(getValue === "" ? cleanSearch : listSearchMovies(getValue), 650));
    }
    e.currentTarget.onkeydown = () => {
      clearTimeout(timeOut);
    }

  }

  return (
      <input onChange={(e) => requestChange(e)} placeholder="Busque um filme por nome, ano ou gênero..." type="text"></input>
  );
  
}

export default Search