import React, { useEffect, useState } from 'react';
import './Pages.scss';
import { useDispatch } from 'react-redux';
import { setCurrentPage } from '../actions/index';

function Pages() {
  const [page, setPage] = useState({ currentPage: 1, numberPage: [1, 2, 3, 4, 5] });
  const dispatch = useDispatch();

  useEffect(() => {

    page.currentPage === 1 ? dispatch(setCurrentPage({ currentPage: 1 })) : dispatch(setCurrentPage({ currentPage: page.currentPage }))

  });

  const alterPage = (number, currentIndex) => {
    let numbers = [];
    
    page.numberPage.map((item) => {
      switch (currentIndex) {
        case 0:
          numbers.push(item - 2)
          break;
        case 1:
          numbers.push(item - 1)
          break;
        case 3:
          numbers.push(item + 1)
          break;
        case 4:
          numbers.push(item + 2)
          break;
        default:
          numbers.push(item)
          
      }
    })

    setTimeout(() => {
      setPage({ currentPage: number, numberPage: numbers })
    }, 10)

  }


  return (
    <>
      <div className="page">
        {page.numberPage.map((number, i) =>
          number > 0 &&
          <button key={i} onClick={() => alterPage(number, i)} className={page.currentPage === number ? 'active' : undefined}>{number}</button>
        )}
      </div>
    </>
  )

}

export default Pages