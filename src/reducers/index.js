import { combineReducers } from "redux";
import { listGenres } from "../actions";

const listMoviesData = (state = { data: false }, action) => {
    switch (action.type) {
        case "LIST_MOVIES":
            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};

const listDetailData = (state = { data: false }, action) => {
    switch (action.type) {
        case "LIST_DETAIL":
            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};


const listSearchData = (state = { data: false }, action) => {
    switch (action.type) {
        case "LIST_SEARCH":
            return {
                ...state,
                data: action.data
            };
        case "CLEAN_SEARCH":
            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};


const listGenresData = (state = { data: false }, action) => {
    switch (action.type) {
        case "LIST_GENRES":

            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};

const currentPage = (state = { data: false }, action) => {
    switch (action.type) {
        case "CURRENT_PAGE":
            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};

export default combineReducers({ listMoviesData, currentPage, listGenresData, listDetailData, listSearchData });
