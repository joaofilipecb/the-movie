import React from 'react';
import Search from './Search/Search';
import MovieBox from './MovieBox/MovieBox';
import Pages from './Pages/Pages';
function App() {
  return (
    <div className="App">
      <div className="split-screen">
        <Search></Search>
        <MovieBox></MovieBox>
        <Pages></Pages>
      </div>
    </div>
  );
}

export default App;
