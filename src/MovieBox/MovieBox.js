import React, { useState, useEffect } from 'react';
import './MovieBox.scss';
import { useSelector, useDispatch } from 'react-redux';
import { listMovies, listGenres } from '../actions/index';
import { Link } from 'react-router-dom';

function MovieBox() {
  const dispatch = useDispatch();
  const listMovie = useSelector(state => state.listMoviesData.data);
  const pageStatus = useSelector(state => state.currentPage.data);
  const genre = useSelector(state => state.listGenresData);
  const search = useSelector(state => state.listSearchData);
  const [averageCount, setAverageCount] = useState();
  const [currentMovies, setCurrentMovies] = useState({ results: [], pageResponse: [] });
  const [requestPage, setRequestPage] = useState([true, 1]);

  !genre.data && dispatch(listGenres())

  useEffect(() => {
    pageStatus && pagination();
  }, [pageStatus]);

  useEffect(() => {
    requestPage && dispatch(listMovies(pageStatus.currentPage === 1 ? 1 : Math.round(pageStatus.currentPage / 5 + 1)));
  }, [requestPage]);

  useEffect(() => {
    listMovie && currentMovies.pageResponse.indexOf(listMovie.page) === -1 && setCurrentMovies({ results: [...currentMovies.results, ...listMovie.results], pageResponse: [...currentMovies.pageResponse, listMovie.page] });
  }, [listMovie]);


  useEffect(() => {
    search.data && search.data !== 'clean' ? setCurrentMovies({ ...currentMovies, results: [...search.data.results], pageResponse: [] }) :
    search.data && setCurrentMovies({ ...currentMovies, results: [...listMovie.results], pageResponse: [] })
  }, [search]);

  const genres = (id) => {

    let name
    genre.data.genres.map((item, i) => {

      if (item.id === id) { name = item.name }

    })

    return name

  }

  const pagination = () => {

    setRequestPage([pageStatus.currentPage * 5 >= currentMovies.results.length, pageStatus.currentPage])

    if (pageStatus.currentPage === 1) {
      setAverageCount({ start: 1, end: 5 })
    } else {
      setAverageCount({ start: pageStatus.currentPage * 5 - 4, end: pageStatus.currentPage * 5 })
    }

  }

  return (
    <>
      {currentMovies.results && currentMovies.results.map((item, i) =>
        i + 1 >= averageCount.start && i + 1 <= averageCount.end &&
        <Link key={i} to={`detail/${item.id}`}>
          <article>
            <figure>
              <img src={`https://image.tmdb.org/t/p/w200/${item.poster_path}`} alt="Movie" />
            </figure>
            <div className="details">
              <div className="header">
                <div className="percent">{item.vote_average * 10}%</div>
                <h3>{item.title}</h3>
                <p className="release-date">{item.release_date}</p>
              </div>
              <p className="desc">{item.overview.substr(0, 200) + "..."}</p>
              <div className="tags">
                {item.genre_ids.map((item, i) =>
                  <span key={i} className="tag">{genres(item)}</span>
                )
                }
              </div>
            </div>
          </article>
        </Link>
      )
      }
    </>
  )
}

export default MovieBox