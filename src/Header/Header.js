import React from 'react';
import './Header.scss';
import { Link } from 'react-router-dom';

function Header() {

  return (
    <header><Link to="/"><h2>Movies</h2></Link></header>
  );
}

export default Header